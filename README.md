# Notes Manager Manual

## Description
Notes Manager is a note management application. It consists of two separate services - [notes-manager-frontend](https://gitlab.com/iliyanvelchev/notes-manager-frontend) and [notes-manager-backend](https://gitlab.com/iliyanvelchev/notes-manager-backend)

## Cannonical source
The source of this manual is hosted [here](https://gitlab.com/iliyanvelchev/notes-manager-manual).

## Requirements
In order to run the system, you will need the following already configured:

- Git
- Docker
- Docker Compose

## Setup Guide

#### Checkout
Checkout this project and then checkout both the frontend and the backend inside of it:
```
$ git clone https://gitlab.com/iliyanvelchev/notes-manager-manual
$ cd notes-manager-manual/
$ git clone https://gitlab.com/iliyanvelchev/notes-manager-frontend
$ git clone https://gitlab.com/iliyanvelchev/notes-manager-backend
```

#### Passwords
We need to set the passwords needed in order to connect to MongoDB, the JWT secret key needed for the authentication.


###### Linux
Append the following to your `.bashrc` file:
```
# Set your own values if you want to
$ export MONGODB_URL=mongodb://mongo/notes
$ export MONGODB_USER=user
$ export MONGODB_PASSWORD=password
$ export JWT_SECRET_KEY=secretkey
```
Open a new terminal tab and run the following to verify that the entries exist:
```
$ printenv
```

###### OS X
Run the following commands:
```
# Set your own values if you want to
$ launchctl setenv MONGODB_URL mongodb://mongo/notes
$ launchctl setenv MONGODB_USER user 
$ launchctl setenv MONGODB_PASSWORD password
$ launchctl setenv JWT_SECRET_KEY secretkey
```
Restart the terminal and run the following to verify that the entries exist:
```
$ printenv
```


#### Build
Build the docker images:
```
# Uses the docker-compose.yaml file in the current directory
$ docker-compose build
```

Run the system:
```
# Uses the docker-compose.yaml file in the current directory
$ docker-compose up -d
```

## Usage

Open the UI on http://localhost:3000 and sign up. Login with the credentials you just provided and start managing your notes!