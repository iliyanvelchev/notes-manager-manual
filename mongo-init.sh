#!/bin/bash
set -e

mongo <<EOF
use notes
db.createUser({
  user:  '$MONGO_INITDB_ROOT_USERNAME',
  pwd: '$MONGO_INITDB_ROOT_PASSWORD',
  roles: [{
    role: 'readWrite',
    db: 'notes'
  }]
})
EOF